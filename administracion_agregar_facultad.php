<?php 
include("mysql_conection.class.php"); 
include("header_footer.class.php");
$ObjMysql = new mysql_conection();
if(-1 == ((isset($_SESSION["Cedula"]) != "")? $_SESSION["Cedula"] : -1))
{
	header('Location: /SAM-UTP/index.php');
}
?>
<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Crear Facultades</title>
	<?php
	$ObjHeaderFooter = new HeadFoot();
	echo $ObjHeaderFooter->EstiloVendor();
	?>
</head>
<body>
<?php echo $ObjHeaderFooter->Header_Ventor()?>
<section>
<div class="container">
	<h1>Registrar Factultades</h1>
	<br/>
	<form action="registrar_facultad.class.php" method="POST" role="form">
		<div class="row">
			<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
				<div class="form-group">
					<label for="">Nombre de la Facultad</label>
					<input type="text" class="form-control" name="txtNombre_facultad">
				</div>
				<div class="form-group">
					<label for="">Nombre de la Facultad</label>
					<textarea style="max-height: 100px; height: 100px; max-width: 100%;width: 100%;" name="textDescripcion" class="form-control" rows="3" required="required"></textarea>
				</div>
				<button type="submit" class="btn btn-primary">Crear Facultad</button>	
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
				<div class="form-group">
					<label class="control-label">Cargar logo facultad</label>
					<input id="input-1a" type="file" class="file" data-show-preview="false">
				</div>
			</div>
		</div>
	</form>	
</div>
<section>
	<br/>	
	<?php echo $ObjHeaderFooter->Footer_HTML(); ?>
</body>
</html>
