<?php
include("mysql_conection.class.php"); 
include("header_footer.class.php");
$ObjMysql = new mysql_conection();
if(-1 == ((isset($_SESSION["Cedula"]) != "")? $_SESSION["Cedula"] : -1))
{
  header('Location: /SAM-UTP/index.php');
}
?>
<!DOCTYPE html>
<html lang="">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Estudiantes</title>
  <?php
  $ObjHeaderFooter = new HeadFoot();
  echo $ObjHeaderFooter->EstiloVendor(); ?>
</head>
<body>
  <!-- Navigation -->
  <?php echo $ObjHeaderFooter->Header_Ventor()?>

  <br>
  <section>
    <div class="container">
      <legend><h3 class="box-title">Opciones</h3></legend>
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Mis cursos</h3>
            </div>
            <div class="box-body">
              <table id='tbMisCursos' class="table table-striped table-hover">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Opcion</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
         <div class="box box-success">
           <div class="box-header">
            <h3 class="box-title"><b>My perfil</b></h3><a class="pull-right"><b>Ajustes</b></a>
          </div>
          <div class="box-body box-profile">
            <img class="profile-user-img img-responsive img-circle" src="img/profile.png" alt="User profile picture">
            <h3 class="profile-username text-center"><?php echo $ObjMysql->BuscarInfoUsuario()->NombreCompleto; ?></h3>
            <p class="text-muted text-center">Estudiante</p>
            <ul class="list-group list-group-unbordered">
              <li class="list-group-item">
                <b>Correo</b> <a class="pull-right"><b id="bcorreo"></b></a>
              </li>
              <li class="list-group-item">
                <b>Contreña</b> <a class="pull-right"><b id="bcontrasena"></b></a>
              </li>
            </ul>
          </div>
        </div>

      </div>
    </div>
    <div class="row">
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
       <div class="box box-danger">
        <div class="box-header">
          <h3 class="box-title">Notificaciones</h3>
        </div>
        <div class="box-body">
          <div class="tab-pane active" id="timeline">
            <ul class="timeline timeline-inverse">

              

            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
     <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Contactanos</h3>
      </div>
      <form role="form">
        <div class="box-body">
          <div class="form-group">
            <label for="exampleInputEmail1">Email</label>
            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Ingrese email">
          </div>
          <div class="form-group">
            <label>CC</label>
            <input class="form-control" id="" required="required">
          </div>
          <div class="form-group">
            <p class="help-block">Descripcion</p>
            <textarea name="" id="input" class="form-control" rows="3" required="required" style="width: 100%; max-width: 100%; max-height: 100px; height: 100px;"></textarea>
          </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
      </form>
    </div>
  </div>
</div>
</section>

<?php echo $ObjHeaderFooter->Footer_HTML(); ?>

</body>
</html>
<style>
  .box-body{
    max-height: 300px; 
    height: 300px;
  }
</style>
<script>
  var JsonMisCursos = <?php echo json_encode($ObjMysql->MisCursos()); ?>;
  var ObjUsuario = <?php echo json_encode($ObjMysql->BuscarInfoUsuario()); ?>;
  
  $(function(){

    for (var i = 0; i < JsonMisCursos.length; i++) {
        if(JsonMisCursos[i].Aprobado == 1)
        {
          $('#tbMisCursos tbody').append(
            '<tr>'+
            '<td>'+ (i+1) +'</td>'+
            '<td>'+ JsonMisCursos[i].NombreCurso +'</td>'+
            '<td><a href="curso.php?Facultad='+ JsonMisCursos[i].idFacultad +'&Curso='+ JsonMisCursos[i].id +'"class="btn btn-sm btn-success">Ver <span style="margin-left:5px;" class="glyphicon glyphicon-new-window"></span></a></td>'+
            '</tr>'
            );
        }
        else
        {
          $('#timeline ul').append(
              '<li>'+
                '<i class="fa fa-user bg-aqua"></i>'+
                '<div class="timeline-item">'+
                  '<h3 class="timeline-header no-border"><a href="#">Curso de: '+ JsonMisCursos[i].NombreCurso +'</a> pendiente por aceptar</h3>'+
                '</div>'+
              '</li>');
        }
    }
    $('#timeline ul').append('<li><i class="fa fa-clock-o bg-gray"></i></li>');
    $('#bcorreo').text(ObjUsuario.Correo);
    $('#bcontrasena').text(ObjUsuario.Contrasena);
  });
</script>