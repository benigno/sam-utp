<?php 
include("mysql_conection.class.php"); 
include("header_footer.class.php");
$ObjMysql = new mysql_conection();
if(-1 == ((isset($_SESSION["Cedula"]) != "")? $_SESSION["Cedula"] : -1))
{
	header('Location: /SAM-UTP/index.php');
}
?>
<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Editar carreras</title>
	<?php
	$ObjHeaderFooter = new HeadFoot();
	echo $ObjHeaderFooter->EstiloVendor();
	?>
</head>
<body>
	<?php echo $ObjHeaderFooter->Header_Ventor(); ?>
	<br/>
	<section>
		<div class="container">
			<h2>Editar carreras</h2>
			<br/>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<table id="tb_Carreras" class="table table-striped table-hover">
						<thead style="background-color: #z">
							<tr>
								<th>#</th>
								<th style="width: 20%;">Facultad</th>
								<th style="width: 20%;">Carrera</th>
								<th>Descripcion</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<div class="modal fade" id="modal_editar_carrera">
				<div class="modal-dialog">
					<div class="modal-content">
						<form action="editar_carreras.class.php" method="POST" role="form">
							<div class="modal-header">
								<h4 class="modal-title">Editar</h4>
							</div>
							<div class="modal-body">
								<input hidden="" type="text" id="txt_idCarrera" name="txt_idCarrera">
								<div class="form-group">
									<label for="">Facultad</label>
									<select name="select_facultades" id="select_facultades" class="form-control" required="required">
										<option value="-1">-----------</option>
									</select>
								</div>
								<div class="form-group">
									<label for="">Carrera</label>
									<input type="text" class="form-control" id="txt_NombreCarrera" name="txt_NombreCarrera">
								</div>
								<div class="form-group">
									<label for="">Descripcion</label>
									<textarea style="width: 100%; max-width: 100%; max-height: 200px; min-height: 200px;" name="txt_descripcion" id="txt_descripcion" class="form-control" rows="3" required="required"></textarea>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" onclick="Cerrar_Modal()" >Cancelar</button>
								<button type="submit" class="btn btn-primary">Guardar Cambios <span style="margin-left: 5px;" class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php echo $ObjHeaderFooter->Footer_HTML(); ?>
</body>
</html>

<script>
	var JsonFacultades = <?php echo json_encode($ObjMysql->BuscarInfoFacultades()); ?>;
	var JsonCarreras = <?php echo json_encode($ObjMysql->BuscarInfoCarreras()); ?>;

	function Cargar_Tabla_Carreras()
	{
		var _html = '';
		for(var i = 0; i < JsonCarreras.length; i++)
		{
			_html += '<tr>';
			_html += '<td>'+ (i + 1) +'</td>';
			_html += '<td>' + JsonCarreras[i].NombreFacultad + '</td>';
			_html += '<td>'+ JsonCarreras[i].NombreCarrera +'</td>';
			_html += '<td>'+ JsonCarreras[i].Descripcion +'</td>';
			_html += '<td><a class="btn btn-primary" data-toggle="modal" onclick="Mostrar_Modal('+ i +','+ JsonCarreras[i].idFacultad +')">Editar ';
			_html += '<span style="margin-left: 5px;" class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>';
			_html += '</tr>';
		}
		$('#tb_Carreras tbody').append(_html);
	}

	function Cargar_Select_Facultades()
	{
		var _html = '';
		for (var i = 0; i < JsonFacultades.length; i++) 
		{
			_html += '<option value="'+ JsonFacultades[i].id +'">'+ JsonFacultades[i].NombreFacultad +'</option>';
		}		
		$('#select_facultades').append(_html);
	}

	function Mostrar_Modal(indice, idFacultad)
	{
		$('#modal_editar_carrera').modal('toggle');
		$('#select_facultades').val(idFacultad);
		$('#txt_NombreCarrera').val(JsonCarreras[indice].NombreCarrera);
		$('#txt_descripcion').text(JsonCarreras[indice].Descripcion);
		$('#txt_idCarrera').val(JsonCarreras[indice].id)
	}

	function Cerrar_Modal()
	{
		$('#modal_editar_carrera').modal('toggle');
		$('#txt_NombreCarrera').val('');
		$('#txt_descripcion').val('');
		$('#select_facultades').val(-1);
		$('#txt_idCarrera').val('');
	}

	$(function(){  
		Cargar_Tabla_Carreras();
		Cargar_Select_Facultades();
	});
</script>