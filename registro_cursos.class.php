<?php 
include("mysql_conection.class.php"); 
$ObjMysql = new mysql_conection();
$conn = new PDO("mysql:host=$ObjMysql->servername;dbname=$ObjMysql->dbName", $ObjMysql->username, $ObjMysql->password);

if(isset($_POST['Revicion']))
{
	if(isset($_POST['Aceptado']))
	{
		echo $_POST['Aceptado'];
		
		if($_POST['Aceptado'] == "1")
		{
			try 
			{
			    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			    $sql = "UPDATE registroestudiantexcursos SET Aprobado=1 WHERE CedulaUsuario='". $_POST["Cedula"] ."'";
			    $conn->exec($sql);
			    header('Location: /SAM-UTP/AdminProfesor.php');
			}
			catch(PDOException $e)
			{
				header('Location: /SAM-UTP/AdminProfesor.php');
			}
			$conn = null;
		}
		else
		{
			try 
			{
			    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			    $sql = "UPDATE registroestudiantexcursos SET Aprobado=0 WHERE CedulaUsuario='". $_POST["Cedula"] ."'";
			    $conn->exec($sql);
			    header('Location: /SAM-UTP/AdminProfesor.php');
			}
			catch(PDOException $e)
			{
				header('Location: /SAM-UTP/AdminProfesor.php');
			}
			$conn = null;
		}
	}
}
else
{
	if(isset($_POST['Eliminar']))
	{
		try 
		{
		    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $sql = "DELETE FROM registroestudiantexcursos WHERE id ";
		    $conn->exec($sql);
		    header('Location: /SAM-UTP/facultad.php?Registro=Correcto');
		}
		catch(PDOException $e)
		{
			header('Location: /SAM-UTP/facultad.php?Registro=Error');
		}
		$conn = null;
	}
	else
	{
		try 
		{
		    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "INSERT INTO registroestudiantexcursos (idCurso, CedulaUsuario, Aprobado, NuevaNotificacion) VALUES (".$_POST["idCurso"].", '".$_SESSION["Cedula"]."', FALSE, TRUE)";
			$conn->exec($sql);
			header('Location: /SAM-UTP/facultad.php?Facultad='.$_POST['idFacultad']);
		}
		catch(PDOException $e)
		{
			header('Location: /SAM-UTP/facultad.php?Facultad='.$_POST['idFacultad'].'&Error=RegistroFallido');
		}
		$conn = null;
	}
}

?>