<?php 
include("mysql_conection.class.php"); 
include("header_footer.class.php");
$ObjMysql = new mysql_conection();
?>
<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Facultades</title>
	<?php
  $ObjHeaderFooter = new HeadFoot();
  echo $ObjHeaderFooter->EstiloVendor(); ?>
</head>
<body>
<?php echo $ObjHeaderFooter->Header_Ventor(); ?>
  <br>
  <section>
    <div class="container">
		<div class="alert alert-success" role="alert" id="div_alert" style="display: none;">
			<h4>Debes iniciar sesión para ver el resto de la información </h4>
		</div>
		<br>
		<div id="div_facultades" class="row">
			
		</div>
		<br>
    </div>
  </section>
  <?php echo $ObjHeaderFooter->Footer_HTML(); ?>
</body>
</html>

<script>
	var JsonFacultades = <?php echo json_encode($ObjMysql->BuscarInfoFacultades()); ?>;
	var Registrado = "<?php echo ((isset($_SESSION["Cedula"]) != "")? $_SESSION["Cedula"] : -1) ?>";

	$(function(){
		var html_ = '';
		for (var i = 0; i < JsonFacultades.length; i++) {
			html_ += '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-5">';
			html_ += '<a href="'+ ((Registrado != -1)? "facultad.php?Facultad="+ JsonFacultades[i].id : "#" ) + '" class="thumbnail">';
			html_ += '<img src="Imagenes/icono_facultad.png" alt="" height="100" width="100">';
			html_ += '</a>';
			html_ += '<h4><a href="'+ ((Registrado != -1)? "facultad.php?Facultad="+ JsonFacultades[i].id : "#" ) + '">';
			html_ += JsonFacultades[i].NombreFacultad + '<span style="margin-left:5px;" class="glyphicon glyphicon-new-window"></span></a></h4>';
			html_ += '</div>';
		}
		$('#div_facultades').append(html_);
		if(Registrado == -1)
			$('#div_alert').show();
	});
</script>