<?php 
include("mysql_conection.class.php"); 
include("header_footer.class.php");
$ObjMysql = new mysql_conection();
if(-1 == ((isset($_SESSION["Cedula"]) != "")? $_SESSION["Cedula"] : -1))
{
	header('Location: /SAM-UTP/index.php');
}
?>
<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Etidar Cursos</title>
	<?php
	$ObjHeaderFooter = new HeadFoot();
	echo $ObjHeaderFooter->EstiloVendor();
	?>
	<link rel="stylesheet" href="js/tinymce/themes/inlite/theme.min.js" media="all">
	<script src="js/tinymce/jquery.tinymce.min.js"></script>
	<script src="js/tinymce/tinymce.min.js"></script>
</head>
<body>
	<?php echo $ObjHeaderFooter->Header_Ventor(); ?>
	<br>
	<section>
		<div class="container">
			<legend><h4>Editar Cursos</h4></legend>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<table id="tb_Cursos" class="table table-striped table-hover">
						<thead style="background-color: #z">
							<tr>
								<th style="width: 5%;">#</th>
								<th style="width: 30%;">Nombre</th>
								<th>Opcion</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>		
				</div>
			</div>
		</div>
	</section>
	<?php echo $ObjHeaderFooter->Footer_HTML(); ?>
</body>
</html>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form action="administracion_curso.class.php" method="POST" role="form">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Editar curso</h4>
					<input type="hidden" name="Editar" id="Editar" class="form-control" value="true">
					<input type="hidden" name="Admin" class="form-control" value="true">
					<input type="hidden" name="txtidCurso" id="txtidCurso" class="form-control">
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							<div class="form-group">
								<label for="">Nombre del curso</label>
								<input type="text" class="form-control" name="txtNombreCurso" id="txtNombreCurso" required="required" ">
							</div>	
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="form-group">
								<label for="">Descripción</label>
								<textarea name="txtDescripcion" id="txtDescripcion" class="form-control" rows="3" style="width: 100%; max-width: 100%; height: 450px; max-height: 450px;"></textarea>
							</div>	
						</div>
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
							<div class="form-group">
								<label for="">Facultad</label>
								<select name="facultad" id="select_facultades" class="form-control" required="required"></select>
							</div>
						</div>	
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
							<div class="form-group" id="div_carreras">
								<label for="">Carrera</label>
								<select disabled="" name="carrera" id="select_carreras" class="form-control" required="required">
									<option value="-1">--Seleccione--</option>
								</select>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
							<div class="form-group" id="div_carreras">
								<label for="">Profesor</label>
								<select disabled="" name="profesor" id="select_profesores" class="form-control" required="required">
									<option value="-1">--Seleccione--</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Guardar</button>
					<button type="submit" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="ModalElimar">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="administracion_curso.class.php" method="POST" role="form">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Alerta!</h4>
				</div>
				<div class="modal-body">
					<div class="alert alert-danger">
	  					<strong>Advertencia!</strong> se va a eliminar el curso.
					</div>
					<input style="display: none;" type="text" name="txtidCurso" id="idCurso_">
					<input style="display: none;" type="text" name="Eliminar" id="Eliminar">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-info" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-success">Aceptar</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	var JsonProfesores = <?php echo json_encode($ObjMysql->BuscarProfesores()); ?>;
	var JsonFacultades = <?php echo json_encode($ObjMysql->BuscarInfoFacultades()); ?>;
	var JsonCarreras = <?php echo json_encode($ObjMysql->BuscarInfoCarreras()); ?>;
	var JsonCursos = <?php echo json_encode($ObjMysql->BuscarCursos()); ?>;
	tinymce.init({ selector:'textarea' });
	$(function(){

		$('#tb_Cursos tbody').empty();
		var html_ = '';	
		for (var i = 0; i < JsonCursos.length; i++) 
		{
			html_ += '<tr>';
			html_ += '<td>'+ (i+1)+'</td>';
			html_ += '<td>'+ JsonCursos[i].NombreCurso +'</td>';

			html_ += '<td>';
			html_ += '<a class="btn btn-info" data-toggle="modal" onclick="Mostrar_Modal('+ i +')">Editar';
			html_ += '<span style="margin-left: 5px;" class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>';
			html_ += '<a style="margin-left: 10px;" class="btn btn-danger" data-toggle="modal" onclick="EliminarCurso('+ i +')">Eliminar';
			html_ += '<span style="margin-left: 5px;" class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>';
			html_ += '</td></tr>';
		}
		$('#tb_Cursos tbody').append(html_);

		var _html = '';
		$('#select_facultades').empty();
		_html += '<option value="-1">--Seleccione--</option>';
		for (var i = 0; i < JsonFacultades.length; i++) 
		{
			_html += '<option value="'+ JsonFacultades[i].id +'">'+ JsonFacultades[i].NombreFacultad +'</option>';
		}
		$('#select_facultades').append(_html);
	});

	$('#select_facultades').change(function() {
			var indice = document.getElementById('select_facultades').value;
			if(indice != -1)
			{
				$('#select_carreras').empty().prop('disabled',false);
				var _html = '<option value="-1">--Selecione--</option>';
				for (var i = 0; i < JsonCarreras.length; i++) 
				{
					if(JsonCarreras[i].idFacultad == indice){
					_html += '<option value="'+ JsonCarreras[i].id +'">'+ JsonCarreras[i].NombreCarrera +'</option>';
					}
				}
				$('#select_carreras').append(_html);
			}
			else
			{
				$('#select_carreras').empty().prop('disabled',true);
				var _html = '<option value="-1">--Selecione--</option>';
				$('#select_carreras').append(_html);

				$('#select_profesores').empty().prop('disabled',true);
				_html = '<option value="-1">--Selecione--</option>';
				$('#select_profesores').append(_html);
			}
	});

	$('#select_carreras').change(function() {
		var indice = document.getElementById('select_carreras').value;
			if(indice != -1)
			{
				$('#select_profesores').empty().prop('disabled',false);
				var _html = '<option value="-1">--Selecione--</option>';
				for (var i = 0; i < JsonProfesores.length; i++) 
				{
					if(JsonProfesores[i].idCarrera == indice)
					{
						_html += '<option value="'+ JsonProfesores[i].id +'">'+ JsonProfesores[i].NombreCompleto +'</option>';
					}
				}
				$('#select_profesores').append(_html);
			}
			else
			{
				$('#select_profesores').empty().prop('disabled',true);
				var _html = '<option value="-1">--Selecione--</option>';
				$('#select_profesores').append(_html);
			}
	});

	function Mostrar_Modal(indice)
	{
		$('#txtidCurso').val(JsonCursos[indice].id);
		$('#txtNombreCurso').val(JsonCursos[indice].NombreCurso);
		
		tinymce.activeEditor.setContent("");
		tinyMCE.activeEditor.selection.setContent(JsonCursos[indice].Descripcion);

		$('#select_facultades').val(JsonCursos[indice].idFacultad);

		$('#select_carreras').empty().prop('disabled',false);
		var _html = '<option value="-1">--Selecione--</option>';
		for (var i = 0; i < JsonCarreras.length; i++) 
		{
			if(JsonCarreras[i].idFacultad == JsonCursos[indice].idFacultad)
			{
				if(JsonCarreras[i].id == JsonCursos[indice].idCarrera)
				{
					_html += '<option selected value="'+ JsonCarreras[i].id +'">'+ JsonCarreras[i].NombreCarrera +'</option>';
				}
				else
				{
					_html += '<option value="'+ JsonCarreras[i].id +'">'+ JsonCarreras[i].NombreCarrera +'</option>';
				}
			}
		}
		$('#select_carreras').append(_html);
		
		$('#select_profesores').empty().prop('disabled',false);
		var _html = '<option value="-1">--Selecione--</option>';
		for (var i = 0; i < JsonProfesores.length; i++) 
		{
			if(JsonProfesores[i].idCarrera == JsonCursos[indice].idCarrera)
			{
				if(JsonProfesores[i].id == JsonCursos[indice].idProfesor)
				{
					_html += '<option selected value="'+ JsonProfesores[i].id +'">'+ JsonProfesores[i].NombreCompleto +'</option>';
				}
				else
				{
					_html += '<option value="'+ JsonProfesores[i].id +'">'+ JsonProfesores[i].NombreCompleto +'</option>';
				}
			}
		}
		$('#select_profesores').append(_html);

		$('#myModal').modal('toggle');
	}

	function EliminarCurso(indice)
	{
		$('#idCurso_').val(JsonCursos[indice].id);
		$('#ModalElimar').modal('toggle');
	}

</script>
<style>
	#myModal .modal-body{
		max-height: calc(100vh - 200px);
		overflow-y: auto;
	}

	#myModal .modal-dialog {
		width: 100%;
		height: 100%;
		margin: 0;
		padding: 0;
	}

	#myModal .modal-content {
		height: auto;
		min-height: 100%;
		border-radius: 0;
	}
</style>