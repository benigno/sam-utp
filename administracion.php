<?php 
include("mysql_conection.class.php"); 
include("header_footer.class.php");
$ObjMysql = new mysql_conection();
if(-1 == ((isset($_SESSION["Cedula"]) != "")? $_SESSION["Cedula"] : -1))
{
	header('Location: /SAM-UTP/index.php');
}
?>
<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Administracion</title>
	<?php
	$ObjHeaderFooter = new HeadFoot();
	echo $ObjHeaderFooter->EstiloVendor();
	?>
</head>
<body>
	<?php echo $ObjHeaderFooter->Header_Ventor()?>
	<br/>
	<section>
		<div class="container">
			<h2>Configuracion y administracion</h2>
			<div class="row">
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<h3>Facultades</h3>
					<div class="list-group">
						<a href="administracion_agregar_facultad.php" class="list-group-item">Agregar Facultad<span style="margin-left: 5px;" class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a>
						<a href="administracion_editar_facultad.php" class="list-group-item">Editar Info Facultades<span style="margin-left: 5px;" class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a>
					</div>
				</div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<h3>Carreras</h3>
					<div class="list-group">
						<a href="administracion_agregar_carrera.php" class="list-group-item">Crear Carrera<span style="margin-left: 5px;" class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a><!--Debe llevar opcion de seleccion de facultad en el formulario-->
						<a href="administracion_editar_carrera.php" class="list-group-item">Editar Carrera<span style="margin-left: 5px;" class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a>
					</div>
				</div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<h3>Profesores</h3>
					<div class="list-group">
						<a href="administracion_crear_profesor.php" class="list-group-item">Registrar Nuevo Profesor<span style="margin-left: 5px;" class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a><!--Debe llevar opcion de seleccion de facultad y carrera en el formulario-->
						<a href="administracion_editar_profesor.php" class="list-group-item">Editar Profesor<span style="margin-left: 5px;" class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a>
					</div>
				</div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<h3>Estudiantes</h3>
					<div class="list-group">
						<a href="administracion_editar_estudiante.php" class="list-group-item">Editar Estudiante<span style="margin-left: 5px;" class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a>
					</div>
				</div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<h3>Cursos</h3>
					<div class="list-group">
						<a href="administracion_crear_curso.php" class="list-group-item">Crear Curso<span style="margin-left: 5px;" class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a>
						<a href="administracion_editar_curso.php" class="list-group-item">Editar Curso<span style="margin-left: 5px;" class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a><!--Debe poder agregar y eliminar archivos de las carpetas de Archivos, Tareas y Proyectos de cualquier Curso-->
					</div>
				</div>
			</div>							
		</div>
	</section>
	<?php echo $ObjHeaderFooter->Footer_HTML(); ?>
</body>
</html>
