<?php 
include("mysql_conection.class.php"); 
include("header_footer.class.php");
$ObjMysql = new mysql_conection();
if(-1 == ((isset($_SESSION["Cedula"]) != "")? $_SESSION["Cedula"] : -1))
{
	header('Location: /SAM-UTP/index.php');
}
?>
<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Crear Profesor</title>
	<?php
	$ObjHeaderFooter = new HeadFoot();
	echo $ObjHeaderFooter->EstiloVendor();
	?>
</head>
<body>
	<?php echo $ObjHeaderFooter->Header_Ventor();?>
	<br/>
	<section>
		<div class="container">
			<h1>Crear Profesor</h1>	
			<br>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<table id="tb_Usuarios" class="table table-striped table-hover">
						<thead style="background-color: #z">
							<tr>
								<th>#</th>
								<th style="width: 20%;">Nombre Completo</th>
								<th>Correo</th>
								<th>Cedula</th>
								<th>Contraseña</th>
								<th>Rol</th>
								<th style="text-align: center; width: 10%;">Opcion</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>		
				</div>
			</div>
			<div class="modal fade" id="modal_editar_usuario">
				<div class="modal-dialog">
					<div class="modal-content">
						<form action="administracion_profesor.class.php" method="POST" role="form" onsubmit="return Validar_Campos();">
							<div class="modal-header">
								<h4 class="modal-title">Crear profesor</h4>
							</div>
							<input hidden="" type="text" name="txtidUsuario" id="txtidUsuario">
							<div class="modal-body">
								<div class="form-group">
									<label for="">Seleccione la facultad</label>
									<select name="select_facultad" id="select_facultades" class="form-control" required="required"></select>
								</div>
								<div class="form-group" id="div_carreras" style="display: none;">
									<label for="">Seleccione la carrera</label>
									<select name="select_carrera" id="select_carreras" class="form-control" required="required"></select>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" onclick="Cerrar_Modal()" >Cancelar</button>
								<button onclick="GuadarCambios()" type="submit" class="btn btn-primary">Guardar Cambios <span style="margin-left: 5px;" class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></button>
							</div>
						</form>
					</div>
				</div>
			</div>
			
		</div>
	</section>
	<?php echo $ObjHeaderFooter->Footer_HTML(); ?>
</body>
</html>

<script>

		var JsonUsuarios = <?php echo json_encode($ObjMysql->BuscarInfoUsuarios()); ?>;
		var JsonFacultades = <?php echo json_encode($ObjMysql->BuscarInfoFacultades()); ?>;
		var JsonCarreras = <?php echo json_encode($ObjMysql->BuscarInfoCarreras()); ?>;


		function Cargar_Tabla_Usuarios(){
			var _html = '';
			for(var i = 0; i < JsonUsuarios.length; i++)
			{
				_html += '<tr>';
				_html += '<td>'+ (i+1) +'</td>';
				_html += '<td>' + JsonUsuarios[i].NombreCompleto + '</td>';
				_html += '<td>' + JsonUsuarios[i].Correo + '</td>';
				_html += '<td>' + JsonUsuarios[i].Cedula + '</td>';
				_html += '<td>' + JsonUsuarios[i].Contrasena + '</td>';
				_html += '<td>'+ JsonUsuarios[i].idRol +'</td>';
				_html += '<td><a class="btn btn-primary" data-toggle="modal" onclick="Mostrar_Modal('+ i +')">Volver Profesor';
				_html += '<span style="margin-left: 5px;" class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>';
				_html += '</tr>';
			}
			$('#tb_Usuarios tbody').append(_html);
		}

		function Mostrar_Modal(indice)
		{
			$('#select_facultades').val(-1);
			$('#select_carreras').empty();
			$('#div_carreras').hide();
			$('#txtidUsuario').val(JsonUsuarios[indice].id);
			$('#modal_editar_usuario').modal('toggle');
		}

		function Cerrar_Modal()
		{
			$('#modal_editar_usuario').modal('toggle');
			$('#select_facultades').val(-1);
			$('#select_carreras').empty();
			$('#div_carreras').hide();
		}

		function Validar_Campos()
		{
			var completo = true;
			if($('#select_facultades').val() == -1)
				completo = false;
			else if($('#select_carreras').val() == -1)
				completo = false;

			return completo;
		}
		
		$(function()
		{  
			Cargar_Tabla_Usuarios(); 

			var _html = '';
			_html += '<option value="-1">--Selecione--</option>';
			for (var i = 0; i < JsonFacultades.length; i++) 
			{
				_html += '<option value="'+ JsonFacultades[i].id +'">'+ JsonFacultades[i].NombreFacultad +'</option>';
			}
			$('#select_facultades').append(_html);
		});

		$('#select_facultades').on('change', function() {
			var indice = document.getElementById('select_facultades').value;
			if(indice != -1)
			{
				$('#div_carreras').show();
				$('#select_carreras').empty();
				var _html = '<option value="-1">--Selecione--</option>';
				for (var i = 0; i < JsonCarreras.length; i++) 
				{
					if(JsonCarreras[i].idFacultad == indice){
					_html += '<option value="'+ JsonCarreras[i].id +'">'+ JsonCarreras[i].NombreCarrera +'</option>';
					}
				}
				$('#select_carreras').append(_html);
			}
			else
			{
				$('#div_carreras').hide();
			}
		});

</script>
