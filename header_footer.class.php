<?php
class HeadFoot
{

	public function Estilo()
	{
		$html = '<link rel="stylesheet" href="css/bootstrap.min.css" media="all">
				<link rel="stylesheet" href="css/bootstrap-theme.min.css" media="all">
				<script src="js/jquery-3.1.1.min.js"></script>
				<script src="js/bootstrap.min.js"></script>
				<script src="js/bootbox.min.js"></script>
				<style>
				body {
					    background-image: url("Imagenes/Background/images_abstract.jpg");
					}
				</style>
				';
			return $html;
	}

	public function Header_HTML()
	{
		$html = '<br><br>
					<nav class="navbar navbar-inverse">
						<div class="container">
						  <div class="navbar-header">
						    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						      <span class="sr-only">Toggle navigation</span>
						      <span class="icon-bar"></span>
						      <span class="icon-bar"></span>
						      <span class="icon-bar"></span>
						    </button>
						    <a class="navbar-brand" href="index.php">SAM-UTP</a>
						  </div>
						  <div class="navbar-collapse collapse">
						    <ul class="nav navbar-nav">
						      <li class="active"><a href="index.php">Inicio</a></li>';
						      if(isset($_SESSION["Registrado"]))
								{
									if($_SESSION["Registrado"] == "true")
									{
										if($_SESSION["idRol"] == "7")
										{
											$html .= '<li><a href="administracion.php" >Administracion</a></li>'; 
										}
										else if($_SESSION["idRol"] == "3")
										{
											$html .= '<li><a href="AdminProfesor.php" >Administracion Profesor</a></li>'; 
										}
										$html .= '<li><a href="usuario.php">Mi Perfil</a></li>';
										$html .= '<li><a href="cerrar_sesion.class.php">Cerrar Sesión</a></li>';
									}
									else
									{
										$html .= '<li><a href="inicio_sesion.php">Iniciar Sesión</a></li>';		
									}
								}
								else
								{
									$html .= '<li><a href="inicio_sesion.php">Iniciar Sesión</a></li>';
								}
						    $html .= '</ul>
						  </div><!--/.nav-collapse -->
						</div>
					</nav>';
		return $html;
	}

	public function Header_Ventor()
	{
		
		$html = '<nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
				        <div class="container">
				            <div class="navbar-header page-scroll">
				                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
				                </button>
				                <a class="navbar-brand" href="#page-top">SAM-UTP</a>
				            </div>
				            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				                <ul class="nav navbar-nav navbar-right">';
						      	if(isset($_SESSION["Registrado"]))
								{
									if($_SESSION["Registrado"] == "true")
									{
										$html .= '<li><a href="facultades.php">Facultades</a></li>';
										if($_SESSION["idRol"] == "7")
										{
											$html .= '<li><a href="administracion.php" >Administracion</a></li>';
										}
										else if($_SESSION["idRol"] == "3")
										{
											$html .= '<li><a href="AdminProfesor.php" >Profesor</a></li>';
										}
										else if($_SESSION["idRol"] == "0")
										{
											$html .= '<li><a href="estudiante.php" >Estudiante</a></li>'; 
										}
										$html .= '<li><a href="cerrar_sesion.class.php">Cerrar Sesión</a></li>';
									}
									else
									{
										$html .= '<li><a href="inicio_sesion.php">Iniciar Sesión</a></li>';		
									}
								}
								else
								{
									$html .= '<li class="page-scroll"><a href="#portfolio">Recursos</a></li>';
									$html .= '<li class="page-scroll"><a href="#about">Nosotros</a></li>';
									$html .= '<li class="page-scroll"><a href="#contact">Contactenos</a></li>';
									$html .= '<li><a href="inicio_sesion.php">Iniciar Sesión</a></li>';
								}       
				         $html .='</ul>
				            </div>
				        </div>
				    </nav>';
		return $html;
	}

	public function Footer_HTML()
	{
		$html = '<footer class="footer">
			      <div class="container">
			        <p class="text-muted">Copyright (c) 2016 Universidad Tecnologica - Lic. de Desarrollo de Software.</p>
			      </div>
			    </footer>';
		return $html;
	}

	public function EstiloVendor()
	{
		$htmlstyle = '<!-- Bootstrap Core CSS -->
	    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	    <!-- Theme CSS -->
	    <link href="css/freelancer.min.css" rel="stylesheet">

	    <!-- Custom Fonts -->
	    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
	    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
	    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  		<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  		<script src="js/jquery-3.1.1.min.js"></script>
  		<script src="js/bootstrap.min.js"></script>
		<script src="js/bootbox.min.js"></script>';
		return $htmlstyle;
	}

	public function EstiloVendorFooter()
	{
		$htmlstyle = '<!-- jQuery -->
	    <script src="vendor/jquery/jquery.min.js"></script>

	    <!-- Bootstrap Core JavaScript -->
	    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

	    <!-- Plugin JavaScript -->
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

	    <!-- Contact Form JavaScript -->
	    <script src="js/jqBootstrapValidation.js"></script>
	    <script src="js/contact_me.js"></script>

	    <!-- Theme JavaScript -->
	    <script src="js/freelancer.min.js"></script>';
	    return $htmlstyle;
	}
}

?>