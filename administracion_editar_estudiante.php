<?php 
include("mysql_conection.class.php"); 
include("header_footer.class.php");
$ObjMysql = new mysql_conection();
if(-1 == ((isset($_SESSION["Cedula"]) != "")? $_SESSION["Cedula"] : -1))
{
	header('Location: /SAM-UTP/index.php');
}
?>
<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Editar estudiantes</title>
	<?php
	$ObjHeaderFooter = new HeadFoot();
	echo $ObjHeaderFooter->EstiloVendor();
	?>
</head>
<body>
<?php echo $ObjHeaderFooter->Header_Ventor(); ?>
<br/>
	<section>
		<div class="container">
			<h2>Editar estudiantes</h2>
			<br/>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<table id="tb_Estudiantes" class="table table-striped table-hover">
						<thead style="background-color: #z">
							<tr>
								<th>#</th>
								<th style="width: 20%;">Nombre</th>
								<th>Cedula</th>
								<th style="width: 20%;">Corrreo</th>
								<th>Contrasena</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	<section>
	<?php echo $ObjHeaderFooter->Footer_HTML(); ?>
</body>
</html>
<div class="modal fade" id="modal_estudiante">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="editar_estudiante.class.php" method="POST" role="form" onsubmit="return Validar_Campos();">
				<div class="modal-header">
					<h4 class="modal-title">Editar</h4>
				</div>
				<div class="modal-body">
					<input hidden="" type="text" id="txtidEstudiante" name="txtidEstudiante">
					<div class="form-group">
						<label for="">Nombre</label>
						<input type="text" name="txtNombreCompleto" id="txtNombreCompleto" class="form-control" value="" >
					</div>
					<div class="form-group">
						<label for="">Correo</label>
						<input type="text" class="form-control" id="txtCorreo" name="txtCorreo">
					</div>
					<div class="form-group">
						<label for="">Contrasena</label>
						<input type="text" class="form-control" id="txtContrasena" name="txtContrasena">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" onclick="Cerrar_Modal()" >Cancelar</button>
					<button type="submit" class="btn btn-primary">Guardar Cambios <span style="margin-left: 5px;" class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></button>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	<?php $datos_ =  json_encode($ObjMysql->BuscarInfoEstudiantes()); ?>
	var JsonEstudiantes = <?php echo (($datos_ != "")? $datos_ : "[]"); ?>

	function Cargar_Tabla_Estudiantes()
	{
		var _html = '';
		for(var i = 0; i < JsonEstudiantes.length; i++)
		{
			_html += '<tr>';
			_html += '<td>'+ (i + 1) +'</td>';
			_html += '<td>' + JsonEstudiantes[i].NombreCompleto + '</td>';
			_html += '<td>'+ JsonEstudiantes[i].Cedula +'</td>';
			_html += '<td>'+ JsonEstudiantes[i].Correo +'</td>';
			_html += '<td>'+ JsonEstudiantes[i].Contrasena +'</td>';
			_html += '<td><a class="btn btn-primary" data-toggle="modal" onclick="Mostrar_Modal('+ i +')">Editar ';
			_html += '<span style="margin-left: 5px;" class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>';
			_html += '</tr>';
		}
		$('#tb_Estudiantes tbody').append(_html);
	}

	function Mostrar_Modal(indice)
	{
		$('#txtidEstudiante').val(JsonEstudiantes[indice].id);
		$('#txtNombreCompleto').val(JsonEstudiantes[indice].NombreCompleto);
		$('#txtCorreo').val(JsonEstudiantes[indice].Correo);
		$('#txtContrasena').val(JsonEstudiantes[indice].Contrasena);
		$('#modal_estudiante').modal('toggle');
	}

	function Cerrar_Modal()
	{
		$('#txtidEstudiante').val('');
		$('#txtNombreCompleto').val('');
		$('#txtCorreo').val('');
		$('#txtContrasena').val('');
		$('#modal_estudiante').modal('toggle');
	}

	function Validar_Campos()
	{
		var Completo = true;
		if($('#txtidEstudiante').val() == "") 
			Completo = false;
		else if($('#txtNombreCompleto').val() == "")
			Completo = false;
		else if($('#txtCorreo').val() == "")
			Completo = false;
		else if($('#txtContrasena').val() == "")
			Completo = false;

		return Completo;
	}

	$(function(){  
		Cargar_Tabla_Estudiantes();
	});
</script>