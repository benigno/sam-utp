<?php
session_start();
class mysql_conection
{
	public $servername = "127.0.0.1";
	public $dbName = "sam_utp";
	public $username = "root";
	public $password = "";
	
	public function BuscarInfoUsuario()
	{
		$DatosUsuario = "";
		$conn_ = new mysqli($this->servername, $this->username, $this->password, $this->dbName);
		return $conn_->query("SELECT * FROM usuarios WHERE Cedula = '". $_SESSION["Cedula"] . "'")->fetch_object();
	}

	public function BuscarInfoFacultades()
	{
		$conn_ = new mysqli($this->servername, $this->username, $this->password, $this->dbName);
		$sql = "SELECT id, NombreFacultad FROM facultad";
		$result = $conn_->query($sql);

		$FacultadesArray = Array();
		if ($result->num_rows > 0) 
		{
			while($fila = $result->fetch_assoc())
			{
				array_push($FacultadesArray, $fila);
			}
			return $FacultadesArray;
		}
		else
		{
			return $FacultadesArray;
		}
		
	}

	public function BuscarInfoUsuarios()
	{
		$conn_ = new mysqli($this->servername, $this->username, $this->password, $this->dbName);
		$sql = "SELECT * FROM usuarios WHERE usuarios.idRol < 2";
		$result = $conn_->query($sql);

		$UsuariosArray = Array();
		if ($result->num_rows > 0) 
		{
			while($fila = $result->fetch_assoc())
			{
				array_push($UsuariosArray, $fila);
			}
			return $UsuariosArray;
		}
		else
		{
			return $UsuariosArray;
		}
	}

	public function BuscarInfoEstudiantes()
	{
		$conn_ = new mysqli($this->servername, $this->username, $this->password, $this->dbName);
		$sql = "SELECT * FROM usuarios WHERE usuarios.idRol < 2";
		$result = $conn_->query($sql);

		$UsuariosArray = Array();
		if ($result->num_rows > 0) 
		{
			while($fila = $result->fetch_assoc())
			{
				array_push($UsuariosArray, $fila);
			}
			return $UsuariosArray;
		}
		else
		{
			return $UsuariosArray;
		}
	}

	public function BuscarProfesores()
	{
		$conn_ = new mysqli($this->servername, $this->username, $this->password, $this->dbName);
		$sql = "SELECT * FROM usuarios WHERE usuarios.idRol = 3";
		$result = $conn_->query($sql);

		$UsuariosArray = Array();
		if ($result->num_rows > 0) 
		{
			while($fila = $result->fetch_assoc())
			{
				array_push($UsuariosArray, $fila);
			}
			return $UsuariosArray;
		}
		else
		{
			return $UsuariosArray;
		}
	}

	public function BuscarInfoCarreras()
	{
		$conn_ = new mysqli($this->servername, $this->username, $this->password, $this->dbName);
		$sql = "SELECT carreras.id, carreras.NombreCarrera, carreras.Descripcion, carreras.idFacultad, facultad.NombreFacultad FROM carreras INNER JOIN facultad ON carreras.idFacultad = facultad.id";
		$result = $conn_->query($sql);

		$CarrerasArray = Array();
		if ($result->num_rows > 0) 
		{
			while($fila = $result->fetch_assoc())
			{
				array_push($CarrerasArray, $fila);
			}
			return $CarrerasArray;
		}
		else
		{
			return $CarrerasArray;
		}	
	}

	public function BuscarCursos()
	{
		$conn_ = new mysqli($this->servername, $this->username, $this->password, $this->dbName);
		$sql = "SELECT * FROM cursos";
		$result = $conn_->query($sql);

		$CursosArray = Array();
		if ($result->num_rows > 0) 
		{
			while($fila = $result->fetch_assoc())
			{
				array_push($CursosArray, $fila);
			}
			return $CursosArray;
		}
		else
		{
			return $CursosArray;
		}	
	}

	public function InfoProfesor()
	{
		$conn_ = new mysqli($this->servername, $this->username, $this->password, $this->dbName);
		$sql = "SELECT id, NombreCompleto, Cedula, Correo, idRol, idFacultad, idCarrera FROM usuarios WHERE Cedula = '". $_SESSION["Cedula"] . "'";
		$result = $conn_->query($sql);

		$Array = Array();
		if ($result->num_rows > 0) 
		{
			while($fila = $result->fetch_assoc())
			{
				array_push($Array, $fila);
			}
			return $Array;
		}
		else
		{
			return $Array;
		}
	}

	public function BuscarCursosxProfesor()
	{
		$conn_ = new mysqli($this->servername, $this->username, $this->password, $this->dbName);
		$sql = "SELECT cursos.id, cursos.NombreCurso, cursos.Descripcion, cursos.idFacultad, cursos.idCarrera, cursos.idProfesor FROM cursos INNER JOIN usuarios ON cursos.idProfesor = usuarios.id WHERE usuarios.Cedula='". $_SESSION["Cedula"] . "'";
		$result = $conn_->query($sql);

		$Array = Array();
		if ($result->num_rows > 0) 
		{
			while($fila = $result->fetch_assoc())
			{
				array_push($Array, $fila);
			}
			return $Array;
		}
		else
		{
			return $Array;
		}	
	}

	//los cursos en los cuales me registre, pero aun no me aprueban
	public function MisCursosRegistrados()
	{
		$conn_ = new mysqli($this->servername, $this->username, $this->password, $this->dbName);
		$sql = "SELECT * FROM registroestudiantexcursos WHERE CedulaUsuario='". $_SESSION["Cedula"] . "'";
		$result = $conn_->query($sql);

		$Array = Array();
		if ($result->num_rows > 0) 
		{
			while($fila = $result->fetch_assoc())
			{
				array_push($Array, $fila);
			}
			return $Array;
		}
		else
		{
			return $Array;
		}		
	}

	//Cursos donde fui acpetado pro el profe o admin
	public function MisCursos()
	{
		$conn_ = new mysqli($this->servername, $this->username, $this->password, $this->dbName);
		$sql = "SELECT cursos.id, cursos.idFacultad, cursos.NombreCurso, registroestudiantexcursos.Aprobado FROM cursos INNER JOIN registroestudiantexcursos ON cursos.id = registroestudiantexcursos.idCurso WHERE registroestudiantexcursos.CedulaUsuario='". $_SESSION["Cedula"] . "'";
		$result = $conn_->query($sql);

		$Array = Array();
		if ($result->num_rows > 0) 
		{
			while($fila = $result->fetch_assoc())
			{
				array_push($Array, $fila);
			}
			return $Array;
		}
		else
		{
			return $Array;
		}		
	}

	public function CargarCurso($idCurso)
	{
		$conn_ = new mysqli($this->servername, $this->username, $this->password, $this->dbName);
		//return $name = $conn_->query("SELECT * FROM cursos WHERE id=$idCurso")->fetch_object()->Descripcion;  
		return $name = $conn_->query("SELECT * FROM cursos WHERE id=$idCurso")->fetch_object();
	}

	public function SolicitudesxCursos()
	{
		$conn_ = new mysqli($this->servername, $this->username, $this->password, $this->dbName);
		$sql = "SELECT registroestudiantexcursos.Aprobado, cursos.id as 'idCurso', cursos.idFacultad, cursos.NombreCurso, usuarios.NombreCompleto, usuarios.id as 'idUsuario', registroestudiantexcursos.id as 'idRegistro', usuarios.Cedula FROM cursos INNER JOIN registroestudiantexcursos ON cursos.id = registroestudiantexcursos.idCurso INNER JOIN usuarios ON usuarios.Cedula=registroestudiantexcursos.CedulaUsuario WHERE cursos.idProfesor=". $this->BuscarInfoUsuario()->id;
		$result = $conn_->query($sql);

		$Array = Array();
		if ($result->num_rows > 0) 
		{
			while($fila = $result->fetch_assoc())
			{
				array_push($Array, $fila);
			}
			return $Array;
		}
		else
		{
			return $Array;
		}	
	}	

}
?>