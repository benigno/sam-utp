<?php
include("mysql_conection.class.php"); 
include("header_footer.class.php");
$ObjMysql = new mysql_conection();
?>
<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SAM-UTP</title>
	<?php
	$ObjHeaderFooter = new HeadFoot();
	echo $ObjHeaderFooter->EstiloVendor(); ?>
</head>
<body>
	<!-- Navigation -->
	<nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header page-scroll">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
				</button>
				<a class="navbar-brand" href="index.php">SAM-UTP</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li>
						<a href="index.php">Inicio</a>
					</li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>

	<!-- Header -->
	<section>
		<div class="container">

			<?php
		 //echo $ObjHeaderFooter->Header_HTML();

			if(isset($_GET["Registro"]))
			{
				if($_GET["Registro"] == "Error")
				{
					echo '<div class="alert alert-danger" role="alert">
					<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
					Error en el registro, por favor intente de nuevo.
				</div>';
			}
			else
			{
				echo '<div class="alert alert-success" role="alert">
				<span class="glyphicon glyphicon glyphicon-ok-sign" aria-hidden="true"></span>
				Registro valido.
			</div>';					
		}
	}
	?>
	<br/>
	<!--TODO:AQUI COLOCAR EL CONTENIDO-->
	<div class="row">
		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
			<form action="validar_registro.class.php" method="POST" role="form">
				<legend>Iniciar Sesión</legend>
				<div class="form-group">
					<label for="">Número Cedula</label>
					<input name="txtUsuario" type="text" class="form-control">
				</div>
				<div class="form-group">
					<label for="">Contraseña</label>
					<input type="password" class="form-control" name="txtContrasena">
				</div>
				<button type="submit" class="btn btn-primary">Entrar</button>
			</form>	
		</div>
		<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">

		</div>
		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
			<form action="registrar_usuario.class.php" method="POST" role="form">
				<legend>Registrar Usuario Nuevo</legend>
				<div class="form-group">
					<label for="">Nombre Completo</label>
					<input type="text" class="form-control" name="txtNombre" required="">
				</div>
				<div class="form-group">
					<label for="">Cedula</label>
					<input type="text" class="form-control" name="txtCedula" required="">
				</div>
				<div class="form-group">
					<label for="">Correo</label>
					<input type="text" class="form-control" name="txtCorreo" required="">
				</div>
				<div class="form-group">
					<label for="">Contraseña</label>
					<input type="password" class="form-control" name="txtContrasena1" required="">
				</div>
				<div class="form-group">
					<label for="">Validar Contraseña</label>
					<input type="password" class="form-control" name="txtContrasena2">
				</div>
				<button type="submit" class="btn btn-primary">Registrar Cuenta</button>
			</form>
		</div>
	</div>
	<br>

	<!--TODO:AQUI COLOCAR EL CONTENIDO-->
</div>
</section>

<!-- Footer -->
<footer class="text-center">
	<div class="footer-above">
		<div class="container">
			<div class="row">
				<div class="footer-col col-md-4">
					<h3>Ubicación</h3>
					<p>3481 Melrose Place
						<br>Beverly Hills, CA 90210</p>
					</div>
					<div class="footer-col col-md-4">
						<h3>Nuestras redes</h3>
						<ul class="list-inline">
							<li>
								<a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
							</li>
							<li>
								<a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
							</li>
							<li>
								<a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
							</li>
							<li>
								<a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-linkedin"></i></a>
							</li>
							<li>
								<a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-dribbble"></i></a>
							</li>
						</ul>
					</div>
					<div class="footer-col col-md-4">
						<h3>About Freelancer</h3>
						<p>Freelance is a free to use, open source Bootstrap theme created by <a href="http://startbootstrap.com">Start Bootstrap</a>.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-below">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						Copyright &copy; Benigno Moreno 2016
					</div>
				</div>
			</div>
		</div>
	</footer>
	<?php echo $ObjHeaderFooter->EstiloVendorFooter(); ?>
</body>
</html>
