<?php 
include("mysql_conection.class.php"); 
include("header_footer.class.php");
$ObjMysql = new mysql_conection();

if(-1 == ((isset($_SESSION["Cedula"]) != "")? $_SESSION["Cedula"] : -1))
{
	header('Location: /SAM-UTP/facultades.php');
}

?>
<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Editar estudiantes</title>
	<?php
	$ObjHeaderFooter = new HeadFoot();
	echo $ObjHeaderFooter->EstiloVendor();
	?>
</head>
<body>
<?php echo $ObjHeaderFooter->Header_Ventor(); ?>
<br/>
<section>
	<div class="container">
		<h3 id="tituloFacultad">Facultad de ...</h3>
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-md-6 col-lg-4">
			<legend>Carreras</legend>
				<div class="form-group">
					<select name="select_Carreras" id="select_Carreras" class="form-control">
					</select>
				</div>
			</div>
			<div class="col-xs-12 col-sm-10 col-md-6 col-lg-4">
			<legend>Profesores</legend>
				<div class="form-group">
					<select name="select_Profesores" id="select_Profesores" class="form-control">
					</select>
				</div>
			</div>
		</div>
		<div id="div_Recursos" class="row">
			
		</div>
	</div>
</section>
	<?php echo $ObjHeaderFooter->Footer_HTML(); ?>
</body>
</html>

<div class="modal fade" id="modalRegistro">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="registro_cursos.class.php" method="POST" role="form">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Registrarse</h4>
				</div>
				<div class="modal-body">
					<div class="media">
					  <div class="media-left media-middle">
					    <a href="#">
					      <img class="media-object" src="imagenes/Background/icono_cursos.png" height="50" width="50">
					    </a>
					  </div>
					  <div class="media-body">
					    <h4 class="media-heading" id="titulo_Curso"></h4>
					  </div>
					</div>
					<input name="idCurso" id="idCurso_" style="display: none;">
					<input name="idFacultad" id="idFacultad_" style="display: none;">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-primary">OK</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="modalEliminar">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="registro_cursos.class.php" method="POST" role="form">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Salir</h4>
				</div>
				<div class="modal-body">
					<div class="media">
					  <div class="media-left media-middle">
					    <a href="#">
					      <img class="media-object" src="imagenes/Background/icono_cursos.png" height="50" width="50">
					    </a>
					  </div>
					  <div class="media-body">
					    <h4 class="media-heading" id="titulo_Curso_"></h4>
					  </div>
					   <input name="Eliminar" id="Eliminar" style="display: none;">
					   <input name="idCurso" id="_idCurso" style="display: none;">
					   <input name="idFacultad" id="_idFacultad" style="display: none;">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-primary">OK</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script>

	var JsonMisCursos = <?php echo json_encode($ObjMysql->MisCursosRegistrados()); ?>;
	var JsonFacultades = <?php echo json_encode($ObjMysql->BuscarInfoFacultades()); ?>;
	var IdFacultad = <?php echo $_GET["Facultad"]; ?>;
	var JsonProfesores = <?php echo json_encode($ObjMysql->BuscarProfesores()); ?>;
	var JsonCarreras = <?php echo json_encode($ObjMysql->BuscarInfoCarreras()); ?>;
	var JsonCursos = <?php echo json_encode($ObjMysql->BuscarCursos()); ?>;
	var Cedula = <?php echo ((isset($_SESSION["Cedula"]) != "")? $_SESSION["Cedula"] : -1) ?>;

	$(function(){
		var indice_ = JsonFacultades.map(function(e){ return Number(e.id); }).indexOf(IdFacultad);
		$('#tituloFacultad').text(JsonFacultades[indice_].NombreFacultad);

		$('#select_Carreras').append('<option value="-1">--Seleccione--</option>');
		for (var i = 0; i < JsonCarreras.length; i++) {
			if(JsonCarreras[i].idFacultad == JsonFacultades[indice_].id){
			$('#select_Carreras').append('<option value="'+ JsonCarreras[i].id +'">'+ JsonCarreras[i].NombreCarrera +'</option>');
			}
		}
		$('#select_Profesores').append('<option value="-1">--Seleccione--</option>').prop('disable', true);
		
	});

	$('#select_Carreras').change(function() 
	{
		$('#div_Recursos').hide();
		$('#select_Profesores').empty();
		$('#select_Profesores').append('<option value="-1">--Seleccione--</option>');
		if($(this).val() != -1){
			for (var i = 0; i < JsonProfesores.length; i++) 
			{
				if(JsonProfesores[i].idCarrera == $(this).val())
				{
					$('#select_Profesores').append('<option value="'+JsonProfesores[i].id +'">'+JsonProfesores[i].NombreCompleto +'</option>');
				}
			}
			$('#select_Profesores').prop('disable', false);
		}
		else{
		$('#select_Profesores').prop('disable', true);
		}
	});

	$('#select_Profesores').change(function() {
		if($(this).val() != -1)
		{
			$('#div_Recursos').empty();
			var _html = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
				_html += '<legend>Recursos</legend>';
				_html += '</div>';
			for (var i = 0; i < JsonCursos.length; i++)
			{
				if(JsonCursos[i].idProfesor == $(this).val()){
					_html += '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">';
					_html += '<div class="thumbnail">';
					_html += '<img src="imagenes/Background/icono_cursos.png" height="150" width="150">';
						_html += '<div class="caption">';
							_html += '<legend id="">Nombre del curso '+ JsonCursos[i].NombreCurso +'</legend>';
							_html += '<div class="row">';
								_html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';

								var existe = JsonMisCursos.map(function(e) {return e.idCurso; }).indexOf(JsonCursos[i].id);
								if(existe == -1){
								_html += '<button data-idfacultad="'+ JsonCursos[i].idFacultad +'" data-id="'+JsonCursos[i].id+'" data-nombrecurso="'+JsonCursos[i].NombreCurso+'" type="button" class="form-control btn-success Registrar" onclick="Registrar(this)">Registrarse</button>';
								}

								_html += '</div>';
								_html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';

								if(existe != -1){
									//_html += '<a href="curso.php?Facultad='+ JsonCursos[i].idFacultad  +'&Curso='+ JsonCursos[i].id +'" class="btn btn-info">Ver</a>';
								}

								_html += '</div>';
								_html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
								if(existe != -1){
									_html += '<button data-idfacultad="'+ JsonCursos[i].idFacultad +'" data-id="'+JsonCursos[i].id+'" data-nombrecurso="'+JsonCursos[i].NombreCurso+'" type="button" class="form-control btn-danger" onclick="SalirDelCurso(this)">Salir</button>';
								}
								_html += '</div>';
							_html += '</div>';
						_html += '</div>';
					_html += '</div>';
				_html += '</div>';
				}
			}
			$('#div_Recursos').append(_html);
			$('#div_Recursos').show();
		}
		else
		{
			$('#div_Recursos').hide();
		}
	});

	function Registrar(thisElement) 
	{
		$('#idCurso_').val($(thisElement).data('id'));
		$('#idFacultad_').val($(thisElement).data('idfacultad'));
		$('#titulo_Curso').text('Registrarse en el curso de ' + $(thisElement).data('nombrecurso'));
		$('#modalRegistro').modal('toggle');
	}

	function SalirDelCurso(thisElement) 
	{
		$('#_idCurso').val($(thisElement).data('id'));
		$('#_idFacultad').val($(thisElement).data('idFacultad'));
		$('#titulo_Curso_').text('Salir del curso ' + $(thisElement).data('nombrecurso'));
		$('#modalEliminar').modal('toggle');
	}

</script>