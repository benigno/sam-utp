<?php
include("mysql_conection.class.php"); 

$ObjMysql = new mysql_conection();

$conn = new PDO("mysql:host=$ObjMysql->servername;dbname=$ObjMysql->dbName", $ObjMysql->username, $ObjMysql->password);

if(!isset($_POST['Editar']))
{
	$idEstudiante_ = $_POST['txtidUsuario'];
	$idFacultad = $_POST['select_facultad'];
	$idCarrera = $_POST['select_carrera'];
	try 
	{
		if(isset($idEstudiante_))
		{
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE usuarios SET IdRol=3, idFacultad=$idFacultad, idCarrera=$idCarrera WHERE id=".$idEstudiante_;
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			header('Location: /SAM-UTP/administracion_crear_profesor.php');
		}
		else
		{
			header('Location: /SAM-UTP/administracion_crear_profesor.php?Error');
		}
	}
	catch(PDOException $e)
	{
		header('Location: /SAM-UTP/administracion_crear_profesor.php?Error');
	}
	$conn = null;
}
else
{
	$idEstudiante_ = $_POST['txtidUsuario'];
	$nombre = $_POST['txtNombre'];
	$contrasena_ = $_POST['txtContrasena'];
	$correo = $_POST['txtCorreo'];
	$idFacultad = $_POST['facultad'];
	$idCarrera = $_POST['carrera'];

	try 
	{
		if(isset($idEstudiante_))
		{
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE usuarios SET NombreCompleto='".$nombre."', Contrasena='".$contrasena_."', Correo='".$correo."', idFacultad=$idFacultad, idCarrera=$idCarrera WHERE id=".$idEstudiante_;
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			header('Location: /SAM-UTP/administracion_editar_profesor.php');
		}
		else
		{
			header('Location: /SAM-UTP/administracion_editar_profesor.php?Error');
		}
	}
	catch(PDOException $e)
	{
		header('Location: /SAM-UTP/administracion_editar_profesor.php?Error');
	}
	$conn = null;
}

?>