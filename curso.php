<?php 
include("mysql_conection.class.php"); 
include("header_footer.class.php");
$ObjMysql = new mysql_conection();
if(-1 == ((isset($_SESSION["Cedula"]) != "")? $_SESSION["Cedula"] : -1))
{
	header('Location: /SAM-UTP/index.php');
}
$ObjCurso = $ObjMysql->CargarCurso($_GET["Curso"]);
?>
<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Curso</title>
	<?php
	$ObjHeaderFooter = new HeadFoot();
	echo $ObjHeaderFooter->EstiloVendor();
	?>
</head>
<body>
<?php echo $ObjHeaderFooter->Header_Ventor(); ?>
<br/>
<section>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<legend><h3><?php echo $ObjCurso->NombreCurso;?></h3></legend>
					<?php echo $ObjCurso->Descripcion;?>	
					<br/>
				<a href="download.php?link=<?php echo $ObjCurso->NombreArchivo;?>"><?php echo $ObjCurso->NombreArchivo;?></a>
			</div>
			<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
			<br/><br/>
				<a href="facultad.php?Facultad=<?php echo $_GET["Facultad"]; ?>" class="btn btn-large btn-block btn-success">Atras</a>
			</div>
		</div>	
		<hr>
		
	</div>
	</section>
	<?php echo $ObjHeaderFooter->Footer_HTML(); ?>
</body>
</html>