<?php 
include("mysql_conection.class.php"); 
include("header_footer.class.php");
$ObjMysql = new mysql_conection();
if(-1 == ((isset($_SESSION["Cedula"]) != "")? $_SESSION["Cedula"] : -1))
{
	header('Location: /SAM-UTP/index.php');
}
?>
<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Editar Facultades</title>
	<?php
	$ObjHeaderFooter = new HeadFoot();
	echo $ObjHeaderFooter->EstiloVendor();
	?>
</head>
<body>
<?php echo $ObjHeaderFooter->Header_Ventor()?>
	<br/>
	<section>
	<div class="container">
		<h2>Editar facultades</h2>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<table id="tb_Facultades" class="table table-striped table-hover">
					<thead style="background-color: #z">
						<tr>
							<th>#</th>
							<th style="width: 20%;">Facultad</th>
							<th>Descripcion</th>
							<th style="width: 10%;">Logo</th>
							<th>Editar</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
		<div class="modal fade" id="modal_editar_facultad">
			<div class="modal-dialog">
				<div class="modal-content">
					<form action="editar_facultad.class.php" method="POST" role="form">
						<div class="modal-header">
							<h4 class="modal-title">Editar</h4>
						</div>
						<input hidden="" type="text" name="txtidFacultad" value="" id="txtidFacultad">
						<div class="modal-body">
							<div class="form-group">
								<label for="">Nombre</label>
								<input type="text" class="form-control" id="txt_NombreFacultad" name="txt_NombreFacultad">
							</div>
							<div class="form-group">
								<label for="">Descripcion</label>
								<textarea style="width: 100%; max-width: 100%; max-height: 200px; min-height: 200px;" name="txt_descripcion" id="txt_descripcion" class="form-control" rows="3" required="required"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" onclick="Cerrar_Modal()" >Cancelar</button>
							<button type="submit" class="btn btn-primary">Guardar Cambios <span style="margin-left: 5px;" class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
	<?php echo $ObjHeaderFooter->Footer_HTML(); ?>
</body>
</html>
<div class="modal fade" id="ModalElimar">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="editar_facultad.class.php" method="POST" role="form">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Alerta!</h4>
				</div>
				<div class="modal-body">
					<div class="alert alert-danger">
	  					<strong>Advertencia!</strong> se va a eliminar la facultad.
					</div>
					<input style="display: none;" type="text" name="idFacultad" id="txtidFacultad_">
					<input style="display: none;" type="text" name="Eliminar" id="Eliminar">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-info" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-success">Aceptar</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script>
	var JsonFacultades = <?php echo json_encode($ObjMysql->BuscarInfoFacultades()); ?>

	function Cargar_Tabla_Facultades(){
		var _html = '';
		for(var i = 0; i < JsonFacultades.length; i++)
		{
			_html += '<tr>';
			_html += '<td>'+ i +'</td>';
			_html += '<td>' + JsonFacultades[i].NombreFacultad + '</td>';
			_html += '<td>'+ JsonFacultades[i].Descripcion +'</td>';
			_html += '<td></td>';
			_html += '<td><a class="btn btn-info" data-toggle="modal" onclick="Mostrar_Modal('+ i +')">Editar ';
			_html += '<span style="margin-left: 5px;" class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>';
			_html += '<br><br><a class="btn btn-danger" data-toggle="modal" onclick="Eliminar('+ i +')">Eliminar ';
			_html += '<span style="margin-left: 5px;" class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>'
			_html += '</td></tr>';
		}
		$('#tb_Facultades tbody').append(_html);
	}

	function Mostrar_Modal(indice)
	{
		$('#modal_editar_facultad').modal('toggle');
		$('#txt_NombreFacultad').val(JsonFacultades[indice].NombreFacultad);
		$('#txt_descripcion').text(JsonFacultades[indice].Descripcion);
		$('#txtidFacultad').val(JsonFacultades[indice].id);
	}

	function Cerrar_Modal()
	{
		$('#modal_editar_facultad').modal('toggle');
		$('#txt_NombreFacultad').val('');
		$('#txt_descripcion').val('');
		$('#txtidFacultad').val('');
	}

	function Eliminar(indice)
	{
		$('#txtidFacultad_').val(JsonFacultades[indice].id)
		$('#ModalElimar').modal('toggle');
	}

	$(function(){  Cargar_Tabla_Facultades(); });
</script>