<?php 
include("mysql_conection.class.php"); 
include("header_footer.class.php");
$ObjMysql = new mysql_conection();
?>
<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Perfil Usuario</title>
	<?php
		$ObjHeaderFooter = new HeadFoot();
		echo $ObjHeaderFooter->EstiloVendor();
	?>
</head>
<body>
	<?php echo $ObjHeaderFooter->Header_Ventor(); ?>
	<br/>
	<section>
		<div class="container">	
		<div role="tabpanel">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active">
					<a href="#home" aria-controls="home" role="tab" data-toggle="tab">Cursos</a>
				</li>
				<li role="presentation">
					<a href="#tab" aria-controls="tab" role="tab" data-toggle="tab">Informacion de Usuario</a>
				</li>
			</ul>
			
			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="home">
					<h3>Tabla de cursos</h3>
					<table class="table table-hover">
						<thead>
							<tr>
								<th>Curso</th>
								<th>Profesor</th>
								<th>Ver</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Programacion 1</td>
								<td>Julio Urriola</td>
								<td> <button type="button" class="btn btn-info">Ver Curso <span class="glyphicon glyphicon-new-window" aria-hidden="true"></span></button> </td>
							</tr>
						</tbody>
					</table>
				</div>
				<div role="tabpanel" class="tab-pane" id="tab">
					<form action="cambiosenusuario.class.php" method="POST" role="form">
						<legend>Detalles</legend>
					
						<div class="form-group">
							<label for="">Correo</label>
							<input disabled="" type="text" class="form-control" id="txtCorreo" name="txtCorreo" placeholder="ejemplo@gmail.com" required="">
						</div>	
						<div class="form-group">
							<label for="">Contrasena</label>
							<input disabled="" type="password" class="form-control" id="txtContrasena" name="txtContrasena" required="">
						</div>

						<div class="form-group divcontrasena">
							<label for="">Nueva Contrasena</label>
							<input disabled="" type="password" class="form-control" id="txtContrasenaNueva1" name="txtContrasena" required="">
						</div>
						<div class="form-group divcontrasena">
							<label for="">Validar Nueva Contrasena</label>
							<input disabled="" type="password" class="form-control" id="txtContrasenaNueva2" name="txtContrasena" required="">
						</div>

						<button type="button" class="btn btn-warning" id="btnhabilitarCampos" >Cambiar Datos</button>
						<button id="btnGuardarCambios" style="display:none;" type="submit" class="btn btn-primary">Guardar Cambios</button>
					</form>
				</div>
			</div>
		</div>
</section>
		<?php echo $ObjHeaderFooter->Footer_HTML(); ?>
	</body>
</html>

<script>
	var Info = <?php echo json_encode($ObjMysql->BuscarInfoUsuario()); ?> 
	var ObjUsuario = { "Cedula": Info.split(',')[0], "Correo" : Info.split(',')[1], "Contrasena" : Info.split(',')[2] };
	$('#txtCorreo').val(ObjUsuario.Correo);
	$('#txtContrasena').val(ObjUsuario.Contrasena);
	$('.divcontrasena').hide();
	$('#btnhabilitarCampos').on('click', function()
	{
		$('#txtCorreo, #txtContrasena').prop('disabled', false);
		$('#btnGuardarCambios').show();
		$('.divcontrasena input').prop('disabled', false);
		$('.divcontrasena').show();		
	});

</script>