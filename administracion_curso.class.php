<?php
include("mysql_conection.class.php"); 

$ObjMysql = new mysql_conection();

$conn = new PDO("mysql:host=$ObjMysql->servername;dbname=$ObjMysql->dbName", $ObjMysql->username, $ObjMysql->password);

if(isset($_POST['Eliminar']))//Elimar
{
	$idCurso = $_POST['txtidCurso'];
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$sql = "DELETE FROM cursos WHERE id=$idCurso";
	$conn->exec($sql);
	if(isset($_POST["esprofesor"]))
    	header('Location: /SAM-UTP/profesor_administrar_cursos.php');
    else
		header('Location: /SAM-UTP/administracion_editar_curso.php');
}
else
{
	if(isset($_POST['Editar']))//actualiza 
	{
		$idCurso = $_POST['txtidCurso'];
		$NombreCurso = $_POST['txtNombreCurso'];
		$Descripcion = $_POST['txtDescripcion'];

		try 
		{
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$nombrearchivo = str_replace(' ', '', $_FILES["fileToUpload"]["name"]);

		    $sql = "UPDATE cursos SET Descripcion='$Descripcion', NombreArchivo='". $nombrearchivo ."' WHERE id=$idCurso";
		    $conn->exec($sql);

			$target_dir = "archivos/";
			$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
			$uploadOk = 1;
			$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
			if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) 
			{
		        echo "El archivo ". basename( $_FILES["fileToUpload"]["name"]). " se ha subido.";

		        rename ("archivo/".$_FILES["fileToUpload"]["name"], $nombrearchivo);
		    } 
		    else 
		    {
		        echo "Lo sentimos, hubo un error subiendo tu archivo.";
		    }

		    if(isset($_POST["Admin"]))
		    	header('Location: /SAM-UTP/administracion_editar_curso.php');
		    else
		   		header('Location: /SAM-UTP/profesor_administrar_cursos.php');
		}
		catch(PDOException $e)
		{
			header('Location: /SAM-UTP/profesor_administrar_cursos.php?Error');
		}
		$conn = null;	
	}
	else//crear un nuevo curso
	{
		$NombreCurso = $_POST['txtNombreCurso'];
		$Descripcion = $_POST['txtDescripcion'];
		$idProfesor = $_POST['profesor'];
		$idFacultad = $_POST['facultad'];
		$idCarrera = $_POST['carrera'];

		try 
		{			
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $sql = "INSERT INTO cursos (NombreCurso, Descripcion, idFacultad, idCarrera, idProfesor, DirectorioArchivos) VALUES ('$NombreCurso', '$Descripcion', $idFacultad, $idCarrera, $idProfesor, '$NombreCurso')";
		    $conn->exec($sql);

		    $directorio_ = "archivos/$NombreCurso";
			// Para crear una estructura anidada se debe especificar
			// el parámetro $recursive en mkdir().
			if(!mkdir($directorio_, 0777, true))
			{
			    echo "Fallo al crear las carpetas...";
			}

		    if(isset($_POST["esprofesor"]))
		    	header('Location: /SAM-UTP/profesor_registrar_curso.php');
		    else
				header('Location: /SAM-UTP/administracion_crear_curso.php');
		}
		catch(PDOException $e)
		{
			if(isset($_POST["esprofesor"]))
		    	header('Location: /SAM-UTP/profesor_registrar_curso.php');
		    else
				header('Location: /SAM-UTP/administracion_crear_curso.php');
		}
		$conn = null;
	}
}

?>