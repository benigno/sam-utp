<?php 
include("mysql_conection.class.php"); 
include("header_footer.class.php");
$ObjMysql = new mysql_conection();
if(-1 == ((isset($_SESSION["Cedula"]) != "")? $_SESSION["Cedula"] : -1))
{
	header('Location: /SAM-UTP/index.php');
}
?>
<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Facultades</title>
	<?php
	$ObjHeaderFooter = new HeadFoot();
	echo $ObjHeaderFooter->EstiloVendor();
	?>
	<link rel="stylesheet" href="js/tinymce/themes/inlite/theme.min.js" media="all">
	<script src="js/tinymce/jquery.tinymce.min.js"></script>
	<script src="js/tinymce/tinymce.min.js"></script>
</head>
<body>
<?php echo $ObjHeaderFooter->Header_Ventor(); ?>
<br/>
<section>
	<div class="container">
	<legend><h3>Cursos</h3></legend>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<table id="tabla_cursos" class="table table-striped table-hover">
					<thead style="background-color: #z">
						<tr>
							<th>#</th>
							<th>Nombre del curso</th>
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
	</section>
	<?php echo $ObjHeaderFooter->Footer_HTML(); ?>
</body>
</html>

<div class="modal fade" id="ModalMostrarCurso">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="administracion_curso.class.php" method="POST" role="form" enctype="multipart/form-data">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Editar curso</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							<div class="form-group">
								<label for="">Nombre del curso</label>
								<input type="text" class="form-control" name="txtNombreCurso" id="txtNombreCurso" required="required" ">
							</div>	
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="form-group">
								<label for="">Descripción</label>
								<textarea name="txtDescripcion" id="txtDescripcion" class="form-control" rows="3" style="width: 100%; max-width: 100%; height: 400px; max-height: 500px;"></textarea>
							</div>	
						</div>
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<div class="form-group">
								<label for="">Archivos</label>
								<label class="control-label">Select File</label>
								<input type="file" name="fileToUpload" id="fileToUpload">
							</div>	
						</div>
					</div>
					<input style="display: none;" type="text" name="Editar" />
					<input style="display: none;" type="text" name="esprofesor" />
					<input style="display: none;" type="text" name="facultad" id="facultad">
					<input style="display: none;" type="text" name="carrera" id="carrera" >
					<input style="display: none;" type="text" name="profesor" id="profesor">
					<input style="display: none;" type="text" name="esprofesor" id="esprofesor">
					<input style="display: none;" type="text" name="txtidCurso" id="idCurso">
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success">Guardar cambios</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal" >Cancelar</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="ModalElimar">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="administracion_curso.class.php" method="POST" role="form">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Alerta!</h4>
				</div>
				<div class="modal-body">
					<div class="alert alert-danger">
	  					<strong>Acevertencia!</strong> se va a eliminar el curso.
					</div>
					<input style="display: none;" type="text" name="esprofesor" />
					<input style="display: none;" type="text" name="txtidCurso" id="idCurso_">
					<input style="display: none;" type="text" name="Eliminar" id="Eliminar">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-info" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-success">Aceptar</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
		tinymce.init({
  selector: 'textarea',
  height: 500,
  theme: 'modern',
  plugins: [
    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'emoticons template paste textcolor colorpicker textpattern imagetools codesample'
  ],
  toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });

	var JsonCursosxProfesor = <?php echo json_encode($ObjMysql->BuscarCursosxProfesor()); ?>;

	$(function(){
		for (var i = 0; i < JsonCursosxProfesor.length; i++) {
			$('#tabla_cursos tbody').append('<tr><td>'+(i+1)+'</td><td>'+ JsonCursosxProfesor[i].NombreCurso +'</td>'+
				'<td><div class="row">'+
				'<div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">'+
				'<button data-indice="'+i+'" type="button" class="form-control btn-info" onclick="Mostrar_Modal(this)" >ver y editar</button>'+
				'</div>'+
				'<div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">'+
				'<button data-indice="'+i+'" onclick="EliminarCurso(this)" type="submit" class="form-control btn-danger">Eliminar<span style="margin-left: 5px;" class="glyphicon'+
				'</div>'+
				'</div></td></tr>');
		}
	});

	function Mostrar_Modal(thisElement)
	{
		$('#txtNombreCurso').val(JsonCursosxProfesor[$(thisElement).data('indice')].NombreCurso);
		tinymce.activeEditor.setContent("");
		tinyMCE.activeEditor.selection.setContent(JsonCursosxProfesor[$(thisElement).data('indice')].Descripcion);
		$('#carrera').val(JsonCursosxProfesor[$(thisElement).data('indice')].idCarrera);
		$('#facultad').val(JsonCursosxProfesor[$(thisElement).data('indice')].idFacultad);
		$('#profesor').val(JsonCursosxProfesor[$(thisElement).data('indice')].idProfesor);
		$('#idCurso').val(JsonCursosxProfesor[$(thisElement).data('indice')].id);
		$('#ModalMostrarCurso').modal('toggle');
	}



	function EliminarCurso(thisElement)
	{
		$('#idCurso_').val(JsonCursosxProfesor[$(thisElement).data('indice')].id);
		$('#ModalElimar').modal('toggle');
	}
</script>

<style>
	#ModalMostrarCurso .modal-body{
		max-height: calc(100vh - 200px);
		overflow-y: auto;
	}

	#ModalMostrarCurso .modal-dialog {
		width: 100%;
		height: 100%;
		margin: 0;
		padding: 0;
	}

	#ModalMostrarCurso .modal-content {
		height: auto;
		min-height: 100%;
		border-radius: 0;
	}
</style>