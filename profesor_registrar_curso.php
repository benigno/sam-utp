<?php 
include("mysql_conection.class.php"); 
include("header_footer.class.php");
$ObjMysql = new mysql_conection();
if(-1 == ((isset($_SESSION["Cedula"]) != "")? $_SESSION["Cedula"] : -1))
{
	header('Location: /SAM-UTP/index.php');
}
?>
<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Registrar Curso</title>
	<?php
	$ObjHeaderFooter = new HeadFoot();
	echo $ObjHeaderFooter->EstiloVendor();
	?>
	<link rel="stylesheet" href="js/tinymce/themes/inlite/theme.min.js" media="all">
	<script src="js/tinymce/jquery.tinymce.min.js"></script>
	<script src="js/tinymce/tinymce.min.js"></script>
</head>
<body>
	<?php echo $ObjHeaderFooter->Header_Ventor(); ?>
	<br/>
	<section>
		<div class="container">
			<form action="administracion_curso.class.php" method="POST" role="form">
				<legend><h3>Registrar Cursos</h3></legend>

				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<div class="form-group">
							<label for="">Nombre del curso</label>
							<input type="text" class="form-control" name="txtNombreCurso" id="txtNombreCurso" required="required" ">
						</div>	
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="form-group">
							<label for="">Descripción</label>
							<textarea name="txtDescripcion" id="txtDescripcion" class="form-control" rows="3" style="width: 100%; max-width: 100%; height: 500px; max-height: 500px;"></textarea>
						</div>	
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<div class="form-group">
							<label for="">Archivos</label>
							<label class="control-label">Select File</label>
							<input id="input-1" type="file" class="file">
						</div>	
					</div>
				</div>
				<input style="display: none;" type="text" name="facultad" id="facultad">
				<input style="display: none;" type="text" name="carrera" id="carrera" >
				<input style="display: none;" type="text" name="profesor" id="profesor">
				<input style="display: none;" type="text" name="esprofesor" id="esprofesor">
				<button id="btnGuardar" type="submit" class="btn btn-primary">Guardar</button>
			</form>
		</div>
	</section>
	<?php echo $ObjHeaderFooter->Footer_HTML(); ?>
</body>
</html>
<script>
	tinymce.init({
  selector: 'textarea',
  height: 500,
  theme: 'modern',
  plugins: [
    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'emoticons template paste textcolor colorpicker textpattern imagetools codesample'
  ],
  toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });
	var JsonProfesor = <?php echo json_encode($ObjMysql->InfoProfesor()); ?>;

	$(function(){
		$('#profesor').val(JsonProfesor[0].id);
		$('#facultad').val(JsonProfesor[0].idFacultad);
		$('#carrera').val(JsonProfesor[0].idCarrera);
	});
</script>
