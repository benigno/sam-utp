<?php 
include("mysql_conection.class.php"); 
include("header_footer.class.php");
$ObjMysql = new mysql_conection();
if(-1 == ((isset($_SESSION["Cedula"]) != "")? $_SESSION["Cedula"] : -1))
{
	header('Location: /SAM-UTP/index.php');
}
?>
<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Profesor</title>
	<?php
	$ObjHeaderFooter = new HeadFoot();
	echo $ObjHeaderFooter->EstiloVendor();
	?>
</head>
<body>
	<?php echo $ObjHeaderFooter->Header_Ventor(); ?>
	<br/>
	<section>
		<div class="container">
			<legend><h3>Administracion Profesor</h3></legend>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

					<div class="box box-primary">
						<div class="box-header">
							<h3 class="box-title"><b>My perfil</b></h3><a class="pull-right"><b>Ajustes</b></a>
						</div>
						<div class="box-body">
							<img class="profile-user-img img-responsive img-circle" src="img/profile.png" alt="User profile picture">
							<h3 class="profile-username text-center"><?php echo $ObjMysql->BuscarInfoUsuario()->NombreCompleto; ?></h3>
							<p class="text-muted text-center">Profesor</p>
							<ul class="list-group list-group-unbordered">
								<li class="list-group-item">
									<b>Correo</b> <a class="pull-right"><b id="bcorreo"><?php echo $ObjMysql->BuscarInfoUsuario()->Correo; ?></b></a>
								</li>
								<li class="list-group-item">
									<b>Contreña</b> <a class="pull-right"><b id="bcontrasena"><?php echo $ObjMysql->BuscarInfoUsuario()->Contrasena; ?></b></a>
								</li>
							</ul>
						</div>
					</div>
					<div class="box box-primary">
						<div class="box-header">
							<h3 class="box-title">Cursos</h3>
						</div>
						<div class="box-body">
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<h4>Nuevo curso</h4>
								<a href="profesor_registrar_curso.php" class="thumbnail" title="Registrar nuevo curso">
									<img src="Imagenes/Background/registrar.png" alt="" height="100" width="100" title="Registrar nuevo curso" >
								</a>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<h4>Editar cursos</h4>
								<a href="profesor_administrar_cursos.php" class="thumbnail" title="Administrar cursos">
									<img src="Imagenes/Background/admin_cursos.png" alt="" height="100" width="100" title="Administrar cursos">
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="box box-success">
						<div class="box-header">
							<h3 class="box-title">Notificaciones</h3>
						</div>
						<div class="box-body">
							<div class="table-responsive">
								<table class="table table-striped table-hover" id="tbNotificaciones">
									<thead>
										<tr>
											<th>Curso</th>
											<th>Usuario</th>
											<th>Accion</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="box box-danger">
						<div class="box-header">
							<h3 class="box-title">Usuarios Registrados</h3>
						</div>
						<div class="box-body">
							<div class="table-responsive">
								<table class="table table-striped table-hover" id="tbUsuariosActivos">
									<thead>
										<tr>
											<th>Curso</th>
											<th>Usuario</th>
											<th>Accion</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>	
			</div>
		</div>	
	</section>
	<?php echo $ObjHeaderFooter->Footer_HTML(); ?>
</body>
</html>

<style>
	.box-body{
		max-height: 400px; 
		height: 400px;
	}
</style>

<script>

	var JsonFacultades = <?php echo json_encode($ObjMysql->BuscarInfoFacultades()); ?>;
	var Registrado = <?php echo ((isset($_SESSION["Cedula"]) != "")? $_SESSION["Cedula"] : -1) ?>;
	var ObjUsuario = <?php echo json_encode($ObjMysql->BuscarInfoUsuario()); ?>;
	var ArraySolicitudesxCursos = <?php echo json_encode($ObjMysql->SolicitudesxCursos()); ?>;
	
	$(function(){
		var _html = '';
		for (var i = 0; i < ArraySolicitudesxCursos.length; i++) 
		{
			if(ArraySolicitudesxCursos[i].Aprobado == 0)
			{
				_html += '<tr>';
				_html += '<td>'+ ArraySolicitudesxCursos[i].NombreCurso +'</td>';
				_html += '<td>'+ ArraySolicitudesxCursos[i].NombreCompleto +'</td>';
				_html += '<td>';
				_html += '<form action="registro_cursos.class.php" method="POST" role="form">';
				_html += '<input name="Revicion" style="display:none;" />';
				_html += '<input value="1" name="Aceptado" style="display:none;" />';
				_html += '<input value="'+ ArraySolicitudesxCursos[i].Cedula +'" name="Cedula" style="display:none;" />';
				_html += '<button type="submit" class="btn btn-success">Aceptar solicitud</button>';
				//_html += '<button value="FALSE" name="Aceptado" type="button" class="btn btn-warning">Cancelar Subcripcion</button>';
				_html += '</form>';
				_html += '</td>';
				_html += '</tr>';
			}
			else
			{
				$('#tbUsuariosActivos tbody').append(
				'<tr>'
				+ '<td>'+ ArraySolicitudesxCursos[i].NombreCurso +'</td>'
				+ '<td>'+ ArraySolicitudesxCursos[i].NombreCompleto +'</td>'
				+ '<td>'
				+ '<form action="registro_cursos.class.php" method="POST" role="form">'
				+ '<input name="Revicion" style="display:none;" />'
				+ '<input value="0" name="Aceptado" style="display:none;" />'
				+ '<input value="'+ ArraySolicitudesxCursos[i].Cedula +'" name="Cedula" style="display:none;" />'
				+ '<button type="submit" class="btn btn-warning">Cancelar Subcripcion</button>'
				+ '</form>'
				+ '</td>'
				+ '</tr>');
			}
		}
		$('#tbNotificaciones').append(_html);


	});

</script>
