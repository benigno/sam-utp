<?php 
include("mysql_conection.class.php"); 
include("header_footer.class.php");
$ObjMysql = new mysql_conection();
if(-1 == ((isset($_SESSION["Cedula"]) != "")? $_SESSION["Cedula"] : -1))
{
	header('Location: /SAM-UTP/index.php');
}
?>
<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Registrar Carreras</title>
	<?php
		$ObjHeaderFooter = new HeadFoot();
		echo $ObjHeaderFooter->EstiloVendor();
	?>
</head>
<body>
<?php echo $ObjHeaderFooter->Header_Ventor(); ?>
<br/>
	<section>
	<div class="container">
		
		<h2>Crear Carrera</h2>
		<br>
		<form action="agregar_carrera.class.php" method="POST" role="form" onsubmit="return Validar_Campos();">
			<legend>Seleccione una facultad</legend>
			<div class="form-group">
				<label>Lista de facultades</label>
				<select name="select_facultades" id="select_facultades" class="form-control" style="width: 300px;" onchange="Mostrar_Formulario()">
					<option value="-1">---------</option>
				</select>
			</div>
			<div style="display: none;" id="div_formulario_carrera">
				<div class="form-group">
					<label for="">Nombre de la Carrera</label>
					<input type="text" class="form-control" name="txtNombre_Carrera" id="txtNombre_Carrera" style="width: 300px;">
				</div>
				<div class="form-group">
					<label for="">Descripcion</label>
					<textarea style="min-width: 100%; max-width: 100%; min-height: 200px; max-height: 200px;" name="txtDescripcion" id="txtDescripcion" class="form-control" rows="3"></textarea>
				</div>
				<button type="submit" class="btn btn-primary">Guardar</button>
				<button class="btn btn-danger" onclick="LimpiarCampos()">Cacelar</button>
			</div>
		</form>
	</div>
	</section>
	<br/>
	<?php 	echo $ObjHeaderFooter->Footer_HTML(); ?>
</body>
</html>
<script>
	var JsonFacultades = <?php echo json_encode($ObjMysql->BuscarInfoFacultades()); ?>

	function Cargar_Lista_Facultades()
	{
		var _html = '';
		for (var i = 0; i < JsonFacultades.length; i++) {
			_html += '<option value="'+ JsonFacultades[i].id +'">'+ JsonFacultades[i].NombreFacultad +'</option>';
		}
		$('#select_facultades').append(_html);
	}

	function Mostrar_Formulario()
	{
		if($('#select_facultades option:selected').val() != -1)
		{
			$('#div_formulario_carrera').show();
		}
		else
		{
			LimpiarCampos();
		}
	}

	function Validar_Campos()
	{
		var completo = true;
		if($('#select_facultades option:selected').val() == -1)
			completo = false;
		else if($('#txtDescripcion').val() == "")
			completo = false;
		else if(('#txtNombre_Carrera').val() == "")
			completo = false;
		return completo;
	}

	function LimpiarCampos()
	{
		$('#select_facultades').val(-1);
		$('#txtNombre_Carrera').val('');
		$('#txtDescripcion').val('');
		$('#div_formulario_carrera').hide();
	}

	$(function(){ Cargar_Lista_Facultades(); });
</script>
