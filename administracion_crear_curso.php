<?php 
include("mysql_conection.class.php"); 
include("header_footer.class.php");
$ObjMysql = new mysql_conection();
if(-1 == ((isset($_SESSION["Cedula"]) != "")? $_SESSION["Cedula"] : -1))
{
	header('Location: /SAM-UTP/index.php');
}
?>
<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Registrar curso</title>
	<?php
	$ObjHeaderFooter = new HeadFoot();
	echo $ObjHeaderFooter->EstiloVendor();
	?>
	<link rel="stylesheet" href="js/tinymce/themes/inlite/theme.min.js" media="all">
	<script src="js/tinymce/jquery.tinymce.min.js"></script>
	<script src="js/tinymce/tinymce.min.js"></script>
</head>
<body>
<?php echo $ObjHeaderFooter->Header_Ventor(); ?>
<section>
	<div class="container">
		<form action="administracion_curso.class.php" method="POST" role="form">
			<br/>
			<legend><h4>Registrar Cursos</h4></legend>
			<div class="row">
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<div class="form-group">
						<label for="">Nombre del curso</label>
						<input type="text" class="form-control" name="txtNombreCurso" id="txtNombreCurso" required="required" ">
					</div>	
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="form-group">
						<label for="">Descripción</label>
						<textarea name="txtDescripcion" id="txtDescripcion" class="form-control" rows="3" style="width: 100%; max-width: 100%; height: 500px; max-height: 500px;"></textarea>
					</div>	
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
					<div class="form-group">
						<label for="">Facultad</label>
						<select name="facultad" id="select_facultades" class="form-control" required="required"></select>
					</div>
				</div>	
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
					<div class="form-group" id="div_carreras">
						<label for="">Carrera</label>
						<select disabled="" name="carrera" id="select_carreras" class="form-control" required="required">
							<option value="-1">--Seleccione--</option>
						</select>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
					<div class="form-group" id="div_carreras">
						<label for="">Profesor</label>
						<select disabled="" name="profesor" id="select_profesores" class="form-control" required="required">
							<option value="-1">--Seleccione--</option>
						</select>
					</div>
				</div>
			</div>
			<button type="submit" class="btn btn-primary">Guardar</button>
		</form>
	</div>
</section>
<?php echo $ObjHeaderFooter->Footer_HTML(); ?>
</body>
</html>
<script>
	var JsonProfesores = <?php echo json_encode($ObjMysql->BuscarProfesores()); ?>;
	var JsonFacultades = <?php echo json_encode($ObjMysql->BuscarInfoFacultades()); ?>;
	var JsonCarreras = <?php echo json_encode($ObjMysql->BuscarInfoCarreras()); ?>;
	
	tinymce.init({
  selector: 'textarea',
  height: 500,
  theme: 'modern',
  plugins: [
    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'emoticons template paste textcolor colorpicker textpattern imagetools codesample'
  ],
  toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });

	$(function(){
		var _html = '';
		$('#select_facultades').empty();
		_html += '<option value="-1">--Seleccione--</option>';
		for (var i = 0; i < JsonFacultades.length; i++) 
		{
			_html += '<option value="'+ JsonFacultades[i].id +'">'+ JsonFacultades[i].NombreFacultad +'</option>';
		}
		$('#select_facultades').append(_html);
	});

	$('#select_facultades').change(function() {
			var indice = document.getElementById('select_facultades').value;
			if(indice != -1)
			{
				$('#select_carreras').empty().prop('disabled',false);
				var _html = '<option value="-1">--Selecione--</option>';
				for (var i = 0; i < JsonCarreras.length; i++) 
				{
					if(JsonCarreras[i].idFacultad == indice){
					_html += '<option value="'+ JsonCarreras[i].id +'">'+ JsonCarreras[i].NombreCarrera +'</option>';
					}
				}
				$('#select_carreras').append(_html);
			}
			else
			{
				$('#select_carreras').empty().prop('disabled',true);
				var _html = '<option value="-1">--Selecione--</option>';
				$('#select_carreras').append(_html);

				$('#select_profesores').empty().prop('disabled',true);
				_html = '<option value="-1">--Selecione--</option>';
				$('#select_profesores').append(_html);
			}
	});

	$('#select_carreras').change(function() {
		var indice = document.getElementById('select_carreras').value;
			if(indice != -1)
			{
				$('#select_profesores').empty().prop('disabled',false);
				var _html = '<option value="-1">--Selecione--</option>';
				for (var i = 0; i < JsonProfesores.length; i++) 
				{
					if(JsonProfesores[i].idCarrera == indice)
					{
						_html += '<option value="'+ JsonProfesores[i].id +'">'+ JsonProfesores[i].NombreCompleto +'</option>';
					}
				}
				$('#select_profesores').append(_html);
			}
			else
			{
				$('#select_profesores').empty().prop('disabled',true);
				var _html = '<option value="-1">--Selecione--</option>';
				$('#select_profesores').append(_html);
			}
	});
</script>