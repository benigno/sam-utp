<?php 
include("mysql_conection.class.php"); 
include("header_footer.class.php");
$ObjMysql = new mysql_conection();
if(-1 == ((isset($_SESSION["Cedula"]) != "")? $_SESSION["Cedula"] : -1))
{
	header('Location: /SAM-UTP/index.php');
}
?>
<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Editar estudiantes</title>
	<?php
	$ObjHeaderFooter = new HeadFoot();
	echo $ObjHeaderFooter->EstiloVendor();
	?>
</head>
<body>
<?php echo $ObjHeaderFooter->Header_Ventor(); ?>
<br/>
<section>
	<div class="container">
		<h3>Editar profesores</h3>
		<br/>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<table id="tb_Profesores" class="table table-striped table-hover">
					<thead style="background-color: #z">
						<tr>
							<th>#</th>
							<th style="width: 20%;">Nombre</th>
							<th>Cedula</th>
							<th>Corrreo</th>
							<th>Contrasena</th>
							<th>Facultad</th>
							<th>Carrera</th>
							<th style="text-align: center;">Acciones</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
		
	</div>
</section>
<?php echo $ObjHeaderFooter->Footer_HTML(); ?>
<div class="modal fade" id="modal_profesores">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="administracion_profesor.class.php" method="POST" role="form" onsubmit="return Validar_Campos();">
				<div class="modal-header">
					<h4 class="modal-title">Editar</h4>
				</div>
				<input hidden="" type="text" name="Editar" id="Editar" value="true">
				<input hidden="" type="text" name="txtidUsuario" id="txtidUsuario">
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-5 col-lg-6">
							<div class="form-group">
								<label for="">Nombre</label>
								<input type="text" name="txtNombre" id="txtNombre" class="form-control" required="required" />
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-5 col-lg-6">
							<div class="form-group">
								<label for="">Contraseña</label>
								<input type="text" name="txtContrasena" id="txtContrasena" class="form-control" required="required" />
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-5 col-lg-12">
							<div class="form-group">
								<label for="">Correo</label>
								<input type="text" name="txtCorreo" id="txtCorreo" class="form-control" required="required" />
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-5 col-lg-6">
							<div class="form-group">
								<label for="">Seleccione la facultad</label>
								<select name="facultad" id="select_facultades" class="form-control" required="required"></select>
							</div>
						</div>	
						<div class="col-xs-12 col-sm-12 col-md-5 col-lg-6">
							<div class="form-group" id="div_carreras">
								<label for="">Seleccione la carrera</label>
								<select name="carrera" id="select_carreras" class="form-control" required="required"></select>
							</div>
						</div>
					</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" onclick="Cerrar_Modal()" >Cancelar</button>
					<button onclick="GuadarCambios()" type="submit" class="btn btn-primary">Guardar<span style="margin-left: 5px;" class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></button>
				</div>
			</form>
		</div>
	</div>
</div>

</body>
</html>

<script>
	var JsonProfesores = <?php echo json_encode($ObjMysql->BuscarProfesores()); ?>;
	var JsonFacultades = <?php echo json_encode($ObjMysql->BuscarInfoFacultades()); ?>;
	var JsonCarreras = <?php echo json_encode($ObjMysql->BuscarInfoCarreras()); ?>;

	$(function(){
		$('#tb_Profesores tbody').empty();
		var _html = '';
		for (var i = 0; i < JsonProfesores.length; i++) 
		{
			_html += '<tr>';
				_html += '<td>'+ (i+1) +'</td>';
				_html += '<td>' + JsonProfesores[i].NombreCompleto + '</td>';
				_html += '<td>' + JsonProfesores[i].Cedula + '</td>';
				_html += '<td>' + JsonProfesores[i].Correo + '</td>';
				_html += '<td>' + JsonProfesores[i].Contrasena + '</td>';

				var _indiceFacultad = JsonFacultades.map(function(e){return e.id;}).indexOf(JsonProfesores[i].idFacultad);
				_html += '<td>' + JsonFacultades[_indiceFacultad].NombreFacultad + '</td>';

				var _indiceCarrera = JsonCarreras.map(function(e){return e.id;}).indexOf(JsonProfesores[i].idCarrera);
				_html += '<td>' + JsonCarreras[_indiceCarrera].NombreCarrera + '</td>';

				_html += '<td style="text-align: center;"><a data-idfacultad="'+ JsonProfesores[i].idFacultad +'" data-idcarrera="'+ JsonProfesores[i].idCarrera +'" class="btn btn-primary" data-toggle="modal" onclick="Mostrar_Modal('+ i +')">Editar';
				_html += '<span style="margin-left: 5px;" class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>';
				_html += '</tr>';
			/*	
			JsonProfesores[i].Cedula
			JsonProfesores[i].Contrasena
			JsonProfesores[i].Correo
			JsonProfesores[i].NombreCompleto
			JsonProfesores[i].id
			JsonProfesores[i].idCarrera
			JsonProfesores[i].idFacultad
			JsonProfesores[i].idRol*/
		}
		$('#tb_Profesores').append(_html);

		$('#select_facultades').empty();
		_html = '';
		_html += '<option value="-1">--Seleccione--</option>';
		for (var i = 0; i < JsonFacultades.length; i++) 
		{
			_html += '<option value="'+ JsonFacultades[i].id +'">'+ JsonFacultades[i].NombreFacultad +'</option>';
		}
		$('#select_facultades').append(_html);
	});

	function Mostrar_Modal(i)
	{
		$('#txtidUsuario').val(JsonProfesores[i].id);

		$('#txtNombre').val(JsonProfesores[i].NombreCompleto);
		$('#txtContrasena').val(JsonProfesores[i].Contrasena);
		$('#txtCorreo').val(JsonProfesores[i].Correo);
		$('#select_facultades').val(event.target.dataset.idfacultad);
		
		$('#select_carreras').empty();
		var _html = '<option value="-1">--Selecione--</option>';
		for (var i = 0; i < JsonCarreras.length; i++)
		{
			if(JsonCarreras[i].idFacultad == event.target.dataset.idfacultad)
			{
				if(event.target.dataset.idcarrera == JsonCarreras[i].id)
				{
					_html += '<option value="'+ JsonCarreras[i].id +'" selected >'+ JsonCarreras[i].NombreCarrera +'</option>';
				}
				else
				{
					_html += '<option value="'+ JsonCarreras[i].id +'">'+ JsonCarreras[i].NombreCarrera +'</option>';
				}
			}
		}
		$('#select_carreras').append(_html);

		$('#modal_profesores').modal('toggle');
	}

	function Cerrar_Modal()
	{
		$('#modal_profesores').modal('toggle');
	}

	function Validar_Campos()
	{
		return true;
	}

	$('#select_facultades').on('change', function() {
			var indice = document.getElementById('select_facultades').value;
			if(indice != -1)
			{
				$('#div_carreras').show();
				$('#select_carreras').empty();
				var _html = '<option value="-1">--Selecione--</option>';
				for (var i = 0; i < JsonCarreras.length; i++) 
				{
					if(JsonCarreras[i].idFacultad == indice){
					_html += '<option value="'+ JsonCarreras[i].id +'">'+ JsonCarreras[i].NombreCarrera +'</option>';
					}
				}
				$('#select_carreras').append(_html);
			}
			else
			{
				$('#div_carreras').hide();
			}
		});

</script>